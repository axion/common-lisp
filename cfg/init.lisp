(let ((*default-pathname-defaults* (make-pathname :defaults *load-pathname* :name nil :type nil)))
  (load "setup.lisp")
  (load "quicklisp.lisp")
  (load "util.lisp"))
